package com.github.leifoolsen.jerseyguice.rest.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.leifoolsen.jerseyguice.domain.HelloBean;
import com.github.leifoolsen.jerseyguice.embeddedjetty.JettyFactory;
import com.github.leifoolsen.jerseyguice.rest.application.ApplicationConfig;
import com.github.leifoolsen.jerseyguice.service.AppLogic;
import com.github.leifoolsen.jerseyguice.service.ToDo;
import com.github.leifoolsen.jerseyguice.service.User;

import org.eclipse.jetty.server.Server;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.Request;

import javax.swing.text.html.parser.Entity;
import javax.validation.constraints.AssertFalse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.StringReader;

public class TodoResourceTest {
    private static final int PORT = 8080;
    private static final String DEFAULT_CONTEXT_PATH = "/";

    private static Server server;
    private static WebTarget target;
 
    private TodoResource todoResource;
    
    @BeforeClass
    public static void setUp() throws Exception {
        // Start the server
        server = JettyFactory.createServer(PORT);
        JettyFactory.start(server);
        assertThat(server.isRunning(), is(true));

        // create the client
        Client c = ClientBuilder.newClient();
        target = c.target(server.getURI()).path(ApplicationConfig.APPLICATION_PATH);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        JettyFactory.stop(server);
    }
    
    @Test
    public void isUserCreatedWithServer() {
    	String userName="test1";
    	
        final Response createResponse = target
        		.path("todoapp")
                .path("users")
                .path(userName)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(null);
        
        assertThat(createResponse.readEntity(String.class), equalTo("OK"));
        
        final Response readResponse = target
        		.path("todoapp")
                .path("users")
                .path(userName)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        
        
        assertThat(readResponse.readEntity(User.class).name, equalTo("test1"));
    }
    
    @Before
    public void Init()
    {
    	todoResource=new TodoResource(new AppLogic());
    	
    }
    
    @Test
    public void isUserCreated() {
    	
        
    	Request request = mock(Request.class);
    	//dont want cachhit
    	when(request.evaluatePreconditions(any(EntityTag.class))).thenReturn(null);
    	
    	//Getting user thats not added yet should fail
    	Response resp=todoResource.getUser("Mark", request);
    	assertThat((String)resp.getEntity(), equalTo("not found"));
    	
    	//createUser just returns OK
    	assertThat(todoResource.newUser("Mark"),  equalTo("OK"));
        
    	//now the user should be found
    	Request request2 = mock(Request.class);
    	when(request2.evaluatePreconditions(any(EntityTag.class))).thenReturn(null);
    	//Getting user thats not added yet should fail
    	Response resp2=todoResource.getUser("Mark", request);
    	
    	assertFalse (((String)resp2.getEntity()).equals("not found"));
    	
        
    }

    @Test
    public void getApplicationWadl() throws Exception {
        final Response response = target
                .path("application.wadl")
                .request(MediaType.APPLICATION_XML)
                .get();

        assertThat(response.getStatus(), equalTo(Response.Status.OK.getStatusCode()));
        String wadl = response.readEntity(String.class);
        assertThat(wadl.length(), greaterThan(0));
    }
    
    public String serializeToString(Object o)
	{
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error while serializing";
		}
	}
}
