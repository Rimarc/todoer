package com.github.leifoolsen.jerseyguice.service;

import java.util.ArrayList;
import java.util.List;



public class VolatileMemmoryImpl implements DataAccess {

	public VolatileMemmoryImpl() {
		Users=new ArrayList<User>();
	}

	List<User> Users;
	
	public void addNewUser(String name) {
		if(userExists(name))
			deleteUser(name);
		Users.add(new User(name));
	}

	public Boolean userExists(String name) {
		for(User user: Users)
		{
			if(user.name.equals(name))
			{
				return true; 
			}
		}
		return false;
	}


	public void addNewToDo(String name, ToDo newToDo) throws Exception {
		for(User user: Users)
		{
			if(user.name.equals(name))
			{
				user.ToDos.add(newToDo);
				return; 
			}
		}
		throw new Exception("Wanted to add new Todo to not existing User");
	}

	public User GetUser(String name) throws Exception {
		for(User user: Users)
		{
			if(user.name.equals(name))
			{
				return user; 
			}
		}
		throw new Exception("Wanted to get User that doesnt exist");
	}

	@Override
	public void close() {
		return;
		
	}

	@Override
	public void deleteUser(String name) {
		try {
			Users.remove(Users.remove(GetUser(name)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void addNewUser(User user) {
		Users.add(user);
		
	}

}
