package com.github.leifoolsen.jerseyguice.service;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;


public class AppLogic {
	
	@Inject
	public AppLogic() {
		this.dao = new VolatileMemmoryImpl();
	}

	private DataAccess  dao;
	public Boolean doesUserExist(String name)
	{
		return dao.userExists(name);
	}
	
	public List<ToDo> getTodosFromUser(String name) throws Exception
	{
		return getUser(name).ToDos;
	}
	
	public User getUser(String name) throws Exception
	{
		return dao.GetUser(name);
	}

	public void addUser(String name) {
		dao.addNewUser(name);
	}
	
	public String serializeToString(Object o)
	{
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error while serializing";
		}
	}

	public void addTodoToUser(String name, ToDo toDo) {
		try {
			dao.addNewToDo(name, toDo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void shutDown() {
		dao.close();
	}
	
	public void Maintaining() {
		System.out.println("Doing some maintainance work every minute!");
	}

	public void deleteUser(String name) {
		dao.deleteUser(name);
		
	}

	public void deleteOneUsersTodo(String name, int index) {
		User user;
		try {
			user = getUser(name);
			user.ToDos.remove(index);
			deleteUser(name);
			addUser(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void addUser(User user) {
		dao.deleteUser(user.name);
		dao.addNewUser(user);
		
	}

	public void replaceTodo(User user, int index, ToDo newTodo) {
		ToDo oldTodo=user.ToDos.get(index);
		oldTodo.caption=newTodo.caption;
		oldTodo.description=newTodo.description;
		oldTodo.Priority=newTodo.Priority;
		
		dao.deleteUser(user.name);
		dao.addNewUser(user);
	}
	
}
