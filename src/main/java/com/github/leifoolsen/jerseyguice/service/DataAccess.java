package com.github.leifoolsen.jerseyguice.service;



public interface DataAccess {
	public void addNewUser(String name);
	
	public void addNewToDo(String name, ToDo newToDo) throws Exception;

	public Boolean userExists(String name);
	
	public User GetUser(String name) throws Exception;

	public void close();

	public void deleteUser(String name);

	public void addNewUser(User user);

}
