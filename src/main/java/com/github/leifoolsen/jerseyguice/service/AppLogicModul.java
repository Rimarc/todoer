package com.github.leifoolsen.jerseyguice.service;

import com.google.inject.Binder;
import com.google.inject.Module;

public class AppLogicModul implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(AppLogic.class);
    }
}
