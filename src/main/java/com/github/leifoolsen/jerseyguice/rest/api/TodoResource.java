package com.github.leifoolsen.jerseyguice.rest.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.time.Instant;
import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.leifoolsen.jerseyguice.service.AppLogic;
import com.github.leifoolsen.jerseyguice.service.ToDo;
import com.github.leifoolsen.jerseyguice.service.User;

@Singleton
@Path("todoapp")
@Produces(MediaType.APPLICATION_JSON)
public class TodoResource {

	private final Logger logger = LoggerFactory.getLogger(getClass());

    private AppLogic appLogic;
    
    
    //should be in something not volatile
    private HashMap<String, Integer> eTagStore;
    
    private ObjectMapper mapper;
    @Inject
    public TodoResource(AppLogic appLogic) {
    	this.appLogic = appLogic;
    	mapper = new ObjectMapper();
        eTagStore = new HashMap<String, Integer>();
   
    }
    
    //should be in an Util class
    private Integer getEtagFor(String name)
    {
    	if(eTagStore.containsKey(name))
    	{
    		return eTagStore.get(name);
    	}
    	else
    	{
    		eTagStore.put(name, 1);
    		return 1;
    	}
    }
    private void incEtagFor(String name)
    {
    	if(eTagStore.containsKey(name))
    	{
    		eTagStore.put(name,(Integer)(eTagStore.get(name)+1));
    	}
    	else
    	{
    		eTagStore.put(name, 2);
    	}
    }
    
    
    @GET
    @Path("users/{user}")
    //public HelloBean sayHello() {
    public Response getUser(@PathParam("user") String user,  @Context Request request) {
    	
    	CacheControl cc = new CacheControl();
        cc.setMaxAge(86400);
        
        
        EntityTag etag = new EntityTag(Integer.toString(getEtagFor(user)));
        
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        // cached resource did change -> serve updated content
        if(builder == null){
        	
        	try {
    			String data = appLogic.serializeToString(appLogic.getUser(user));
    			builder = Response.ok(data);
                builder.tag(etag);
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			String data = "not found";
    			builder = Response.ok(data);
                builder.tag(etag);
    		}
        }
        builder.cacheControl(cc);
        return builder.build();
    }
    
    @GET
    @Path("users/{User}/todos/{index}")
    public Response getTodo(@PathParam("User") String user,@PathParam("index") int index, @Context Request request) {
    	CacheControl cc = new CacheControl();
        cc.setMaxAge(86400);
        EntityTag etag = new EntityTag(Integer.toString(getEtagFor(user)));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if(builder == null){
        	
        	try {
    			String data = appLogic.serializeToString(appLogic.getUser(user).ToDos.get(index));
    			builder = Response.ok(data);
                builder.tag(etag);
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			return Response.serverError().build();
    		}
        }
        builder.cacheControl(cc);
        return builder.build();
        
    }
    
    @GET
    @Path("users/{user}/todos")
    public Response getTodos(@PathParam("user") String user,  @Context Request request) {
    	CacheControl cc = new CacheControl();
        cc.setMaxAge(86400);
        EntityTag etag = new EntityTag(Integer.toString(getEtagFor(user)));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if(builder == null){
        	
        	try {
    			String data = appLogic.serializeToString(appLogic.getUser(user).ToDos);
    			builder = Response.ok(data);
                builder.tag(etag);
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			return Response.serverError().build();
    		}
        }
        builder.cacheControl(cc);
        return builder.build();
    }
    
    @DELETE
    @Path("users/{User}")
    public String deleteUser(@PathParam("User") String user) {
        appLogic.deleteUser(user);
        incEtagFor(user);
        return "OK";
    }
    
    @DELETE
    @Path("users/{User}/todos")
    public String deleteUsersTodos(@PathParam("User") String user) {
        appLogic.deleteUser(user);
        appLogic.addUser(user);
        incEtagFor(user);
        return "OK";
    }
    
    @DELETE
    @Path("users/{User}/todos/{index}")
    public String deleteOneUsersTodo(@PathParam("User") String user,@PathParam("index") int index) {
        appLogic.deleteOneUsersTodo(user, index);
        incEtagFor(user);
        return "OK";
    }
    
    @POST
    @Path("users/{newUser}")
    public String newUser(@PathParam("newUser") String newUser) {
        appLogic.addUser(newUser);
        incEtagFor(newUser);
        return "OK";
    }
    
    @POST
    @Path("users/{user}/todos")
    @Consumes(MediaType.APPLICATION_JSON)
    public String addTodo(@PathParam("user") String username, @Context HttpServletRequest request) {
        try {
			User user=appLogic.getUser(username);
			
			try {
				user.ToDos.add(mapper.readValue(request.getInputStream(), ToDo.class));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "error while derializing";
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error at POST {user}/todos";
		}
        incEtagFor(username);
		return "OK Todo added";
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("users/{User}/todos/{index}")
    public String ReplaceTodo(@PathParam("User") String name,@PathParam("index") int index, @Context HttpServletRequest request) {
    	User user;
		try {
			user = appLogic.getUser(name);
			ToDo newTodo;
			try {
				newTodo = mapper.readValue(request.getInputStream(), ToDo.class);

		    	appLogic.replaceTodo(user, index, newTodo);
		    	incEtagFor(name);
		        return "OK";
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "error while deserializing";
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return "Cant Get User";
		}
		
    }
    
    public String convertStreamToString(InputStream is) throws IOException {
        // To convert the InputStream to String we use the
        // Reader.read(char[] buffer) method. We iterate until the
        // Reader return -1 which means there's no more data to
        // read. We use the StringWriter class to produce the string.
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        }
        return "";
    }
    

}