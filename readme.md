* Build project: *mvn clean install -U*
* Start Jetty from project folder: *mvn exec:java*
* ... or execute main method in `com.github.leifoolsen.jerseyguice.main.Main` from your IDE
* Application.wadl: *http://localhost:8080/api/application.wadl*

Nutzung:

POST auf http://localhost:8080/api/todoapp/users/maik erzeugt user maik
GET auf http://localhost:8080/api/todoapp/users/maik gibt kompletten User sammt Todos zurück
DELETE auf http://localhost:8080/api/todoapp/users/maik löscht den User maik

POST auf http://localhost:8080/api/todoapp/users/maik/todos added ein in JSON formatiertes Todo zu maiks todos wie z.B.
{
"user": "maik",
"caption": "newWOW2222",
"description": "Schon erreicht",
"Priority": 10
}
GET auf http://localhost:8080/api/todoapp/users/maik/todos gibt die liste an Todos zurück
DELETE löscht die Liste aber nicht den User

GET auf http://localhost:8080/api/todoapp/users/maik/todos/{index} gibt spizielles todo zurück
PUT auf http://localhost:8080/api/todoapp/users/maik/todos/{index} ersetzt ein element durch ein neues aus JSON (wenn eins da war)
{
"user": "maik",
"caption": "newWOW2222",
"description": "Schon erreicht",
"Priority": 10
}
DELETE auf http://localhost:8080/api/todoapp/users/maik/todos/{index} löscht das element...

* Import project into your favourite IDE
* Open `HelloResourceTest.java` to start exploring code
